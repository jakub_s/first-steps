package org.sii.module3.task

import spock.lang.Specification

class Tasks3Test extends Specification {

    private tasks3 = new Tasks3()

    def createTestOutput() {
        ByteArrayOutputStream bo = new ByteArrayOutputStream()
        System.setOut(new PrintStream(bo))

        return bo
    }

    def "printN should be print numbers up to n"() {
        setup:
        ByteArrayOutputStream bo = createTestOutput()

        when:
        tasks3.printN(10)
        bo.flush()
        def allWrittenLine = bo.toString()

        then:
        allWrittenLine == """\
                            1
                            2
                            3
                            4
                            5
                            6
                            7
                            8
                            9
                            10
                             """.stripIndent()
    }

    def "should return sum of n numbers"() {
        expect:
        tasks3.sumOfInt(arg) == result

        where:
        arg | result
        1   | 1
        2   | 3
        5   | 15
        123 | 7626
    }

    def "should print multiplications for n"() {
        setup:
        def output = createTestOutput()

        when:
        tasks3.printMultiplications(7)
        output.flush()

        then:
        output.toString() == """\
                            1 x 7 = 7
                            2 x 7 = 14
                            3 x 7 = 21
                            4 x 7 = 28
                            5 x 7 = 35
                            6 x 7 = 42
                            7 x 7 = 49
                             """.stripIndent()
    }

    def "should find number in array"() {
        expect:
        tasks3.isInArray(arg, arr) == result

        where:
        arg | arr                || result
        2   | [1, 2, 3] as int[] || true
        6   | [1, 2, 3] as int[] || false
    }

    def "should reverse given word"() {
        expect:
        tasks3.reverseWord(arg) == result

        where:
        arg                                | result
        "kajak"                            | "kajak"
        "czarodziej"                       | "jeizdorazc"
        "konstantynopolitańczykiewiczówna" | "anwózciweikyzcńatiloponytnatsnok"
    }

    def "sum of events should return correct sum"() {
        expect:
        tasks3.sumOfEvens(arr) == result

        where:
        arr                        || result
        [1, 2, 3] as int[]         || 2
        [4, 12, 654, 111] as int[] || 670
    }

    def "should print square"() {
        setup:
        def output = createTestOutput()

        when:
        tasks3.printSquare(5)
        output.flush()

        then:
        output.toString() == """-----
-----
-----
-----
-----
"""
    }

    def "should print triangle"() {
        setup:
        def output = createTestOutput()

        when:
        tasks3.printTriangle(9)
        output.flush()

        then:
        output.toString() == """-
--
---
----
-----
------
-------
--------
---------
"""
    }

}
