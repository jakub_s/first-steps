package org.sii.module2.task

import org.sii.module2.task.operations.MyValue
import org.sii.module2.task.operations.Operations
import spock.lang.Specification

class OperationsTest extends Specification {

    def "Task 1: test assign operation"() {
        expect:
        operationClass.setAmount(expectedAmount)
        operationClass.setName(expectedName)

        operationClass.getAmount() == expectedAmount
        operationClass.getName() == expectedName

        where:
        operationClass = new Operations()
        expectedAmount = 10
        expectedName = "myName"
    }

    def "Task 2: text class assign operator"() {
        expect:
        operationClass.setMyValue(myValue)

        operationClass.getMyValue() == myValue
        myValueField.getType() == MyValue.class

        where:
        operationClass = new Operations()
        myValue = new MyValue()
        myValueField = operationClass.getClass().getDeclaredField("myValue")
    }

    def "Task 3: add constructor"() {
        expect:
        operationClass.getAmount() == exepctedAmount
        operationClass.getName() == expectedName
        operationClass.getMyValue() == expectedMyValue

        where:
        exepctedAmount = 10
        expectedName = "my-name"
        expectedMyValue = new MyValue()
        operationClass = new Operations(exepctedAmount, expectedName, expectedMyValue)
    }
}
