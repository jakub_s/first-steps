package org.sii.module2.task

import org.sii.module2.task.operations.Relations
import spock.lang.Specification

class RelationsTest extends Specification{

    def "Task 1: implement methods with simple comparision"() {
        expect:
        relationsClass.areEqual(1, 1)
        !relationsClass.areEqual(1, 4)
        !relationsClass.areNotEqual(1, 1)
        relationsClass.areNotEqual(5, 1)
        relationsClass.firstIsGreater(6, 1)
        !relationsClass.firstIsGreater(5, 10)
        !relationsClass.firstIsGreater(5, 5)
        relationsClass.secondIsGreaterOfEqual(2, 5)
        relationsClass.secondIsGreaterOfEqual(5, 5)
        !relationsClass.secondIsGreaterOfEqual(50, 15)

        where:
        relationsClass = new Relations()
    }

    def "Task 2: implement methods with logical operations"() {
        expect:
        relationsClass.isWithin(3, 1, 5)
        relationsClass.isWithin(1, 1, 5)
        relationsClass.isWithin(5, 1, 5)
        !relationsClass.isWithin(6, 1, 5)
        !relationsClass.isWithin(0, 1, 5)
        relationsClass.isOutside(0, 1, 5)
        relationsClass.isOutside(6, 1, 5)
        relationsClass.isOutside(1, 1, 5)
        relationsClass.isOutside(5, 1, 5)
        !relationsClass.isOutside(3, 1, 5)

        where:
        relationsClass = new Relations()
    }
}
