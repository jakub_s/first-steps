package org.sii.module1

import spock.lang.Specification

import java.lang.reflect.Method
import java.lang.reflect.Modifier

class Module1Tests extends Specification {

    def "Task1: create class with specified fields"() {
        expect:
        def privateInt = person.getClass().getDeclaredField("field1")
        def publicInteger = person.getClass().getDeclaredField("field2")
        def protectedLong = person.getClass().getDeclaredField("field3")
        def packageString = person.getClass().getDeclaredField("field4")

        Modifier.isPrivate(privateInt.getModifiers())
        privateInt.getType() == int.class

        Modifier.isPublic(publicInteger.getModifiers())
        publicInteger.getType() == Integer.class

        Modifier.isProtected(protectedLong.getModifiers())
        protectedLong.getType() == long.class

        // zero 0 is default package-private
        packageString.getModifiers() == 0
        packageString.getType() == String.class

        where:
        person = loadClass("org.sii.module1.FieldsExample").newInstance()
    }

    def "Task2: create class with specified constructors"() {
        expect:
        org.sii.module1.ConstructorExample.class.getConstructor()
        org.sii.module1.ConstructorExample.class.getConstructor(String.class)
    }

    def "Task3: constructor should assign given value to class field"() {
        expect:
        def textField = constructorExample.getClass().getDeclaredField("text")
        textField.setAccessible(true)

        constructorExample.text == expecetedValue
        constructorExampleDefault.text == "DEFAULT_VALUE"

        where:
        expecetedValue = "someText"
        constructorExample = loadClass("org.sii.module1.ConstructorExample").newInstance(expecetedValue)
        constructorExampleDefault = loadClass("org.sii.module1.ConstructorExample").newInstance()
    }

    def "Task4: create class with specified methods"() {
        expect:
        def publicVoidParameterlessMethod = methodsExample.getClass().getDeclaredMethod("methodOne")
        publicVoidParameterlessMethod.returnType == void.class
        Modifier.isPublic(publicVoidParameterlessMethod.getModifiers())

        def privateStringReturningMethod = methodsExample.getClass().getDeclaredMethod("methodTwo")
        privateStringReturningMethod.returnType == String.class
        Modifier.isPrivate(privateStringReturningMethod.getModifiers())

        def addingMethod = methodsExample.getClass().getDeclaredMethod("methodThree", int.class, int.class)
        addingMethod.returnType == int.class
        Modifier.isPublic(addingMethod.getModifiers())
        addingMethod.invoke(methodsExample, 2, 1) == 3


        where:
        methodsExample = loadClass("org.sii.module1.MethodsExample").newInstance()
    }

    def "Task5: create class with static fields/methods"() {
        expect:
        def staticField = staticsExample.getClass().getDeclaredField("staticField")
        staticField.getType() == int.class
        Modifier.isStatic(staticField.getModifiers())
        Modifier.isPublic(staticField.getModifiers())

        def staticMethod = staticsExample.getClass().getDeclaredMethod("staticMethod")
        Modifier.isStatic(staticMethod.getModifiers())
        Modifier.isPublic(staticMethod.getModifiers())
        staticMethod.returnType == int.class

        where:
        staticsExample = loadClass("org.sii.module1.StaticsExample").newInstance()
    }

    def "Task6: create class with final fields/methods"() {
        def finalField = finalsExample.getClass().getDeclaredField("finalInt")
        Modifier.isPrivate(finalField.getModifiers())
        Modifier.isFinal(finalField.getModifiers())
        finalField.getType() == int.class

        def finalMethod = finalsExample.getClass().getDeclaredMethod("finalMethod")
        Modifier.isFinal(finalMethod.getModifiers())
        Modifier.isPublic(finalMethod.getModifiers())
        finalMethod.getReturnType() == String.class

        where:
        finalsExample = loadClass("org.sii.module1.FinalsExample").newInstance()
    }

    private Class<?> loadClass(String fullClassName) {
        this.getClass().getClassLoader().loadClass(fullClassName)
    }
}
