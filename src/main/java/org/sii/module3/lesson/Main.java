package org.sii.module3.lesson;

import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        DataProvider dataProvider = new DataProvider();
        List<String> city = dataProvider.getCity();
        List<String> date = dataProvider.getDate();
        List<String> time = dataProvider.getTime();
        List<Double> temperature = dataProvider.getTemperature();
        List<Integer> windSpped = dataProvider.getWindSpped();
        List<Integer> windDirection = dataProvider.getWindDirection();
        List<Double> humidity = dataProvider.getHumidity();
        List<Double> rainfall = dataProvider.getRainfall();
        List<Double> pressure = dataProvider.getPressure();

        System.out.println("-- city--");
        System.out.println(city);
        System.out.println("-- date--");
        System.out.println(date);
        System.out.println("-- time--");
        System.out.println(time);
        System.out.println("-- temperature--");
        System.out.println(temperature);
        System.out.println("-- windSpped--");
        System.out.println(windSpped);
        System.out.println("-- windDirection--");
        System.out.println(windDirection);
        System.out.println("-- humidity--");
        System.out.println(humidity);
        System.out.println("-- rainfall--");
        System.out.println(rainfall);
        System.out.println("-- pressure--");
        System.out.println(pressure);

        // temp - avg, min, max
        // city - starts with
    }
}
