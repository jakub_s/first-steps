package org.sii.module3.lesson;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataProvider
{
    private final static String DATA_SOURCE_PATH = "/pogoda.csv";
    private static final int CITY_IDX = 1;
    private static final int DATE_IDX = 2;
    private static final int TIME_IDX = 3;
    private static final int TEMP_IDX = 4;
    private static final int WIND_SPPED_IDX = 5;
    private static final int WIND_DIRECTION_IDX = 6;
    private static final int HUMIDITY_IDX = 7;
    private static final int RAINFALL_IDX = 8;
    private static final int PRESSURE_IDX = 9;

    private Path dataPath;

    public DataProvider()
    {
        try
        {
            URL resource = this.getClass().getResource(DATA_SOURCE_PATH);
            this.dataPath = Paths.get(resource.toURI());
        } catch (URISyntaxException e)
        {
            throw new IllegalStateException(e);
        }
    }

    public List<String> getCity()
    {
        return fetchData(CITY_IDX, city -> city.replaceAll("\"", ""));
    }

    public List<String> getDate()
    {
        return fetchData(DATE_IDX);
    }

    public List<String> getTime()
    {
        return fetchData(TIME_IDX);
    }

    public List<Double> getTemperature()
    {
        return fetchData(TEMP_IDX, Double::parseDouble);
    }

    public List<Integer> getWindSpped()
    {
        return fetchData(WIND_SPPED_IDX, Integer::parseInt);
    }

    public List<Integer> getWindDirection()
    {
        return fetchData(WIND_DIRECTION_IDX, Integer::parseInt);
    }

    public List<Double> getHumidity()
    {
        return fetchData(HUMIDITY_IDX, Double::parseDouble);
    }

    public List<Double> getRainfall()
    {
        return fetchData(RAINFALL_IDX, Double::parseDouble);
    }

    public List<Double> getPressure()
    {
        return fetchData(PRESSURE_IDX, Double::parseDouble);
    }

    private List<String> fetchData(int dataIndex)
    {
        return fetchData(dataIndex, Function.identity());
    }

    private <T> List<T> fetchData(int dataIndex, Function<String, T> mappingFunction)
    {
        return readFile()
                .map(line -> fetchData(line, dataIndex))
                .filter(data -> data != null && !data.isEmpty())
                .map(mappingFunction)
                .collect(Collectors.toList());
    }

    private String fetchData(String line, int dataIndex)
    {
        String[] split = line.split(",");

        return split.length == dataIndex ? "": split[dataIndex];
    }

    private Stream<String> readFile()
    {
        try
        {
            return Files
                    .lines(dataPath)
                    .skip(1);
        } catch (IOException e)
        {
            throw new RuntimeException("OOps", e);
        }
    }
}
