package org.sii.module3.task;

public class Tasks3
{
    // 1
    public void printN(int n)
    {
        // implementation here
    }

    // 2
    public int sumOfInt(int n)
    {
        // implementation here
        return 0;
    }

    // 3
    public void printMultiplications(int n)
    {
        // implementation here
    }

    // 4
    public boolean isInArray(int arg, int[] array)
    {
        // implementation here
        return false;
    }

    // 5
    public String reverseWord(String word)
    {
        // implementation here
        return "";
    }

    // 6
    public int sumOfEvens(int[] array)
    {
        // implementation here
        return 0;
    }

    // 7
    public void printSquare(int size)
    {
        // implementation here
    }

    // 8
    public void printTriangle(int height)
    {
        // implementation here
    }

    public static void main(String[] args)
    {
        // you can test your implementation in here
        Tasks3 tasks3 = new Tasks3();
    }

}
