package org.sii.module4.lesson.extend;

public class LazyDog extends Dog
{
    public LazyDog(String name)
    {
        super(name);
    }

    @Override
    public void fetch()
    {
        System.out.println(getName() + ": Nope..");
    }
}
