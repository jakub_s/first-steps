package org.sii.module4.lesson.extend;

public class Dog extends Animal
{
    public Dog(String name)
    {
        super(name);
    }

    public Dog()
    {
        System.out.println("In Dog class");
    }

    public void fetch()
    {
        System.out.println(getName() + ": I am fetching!");
    }
}
