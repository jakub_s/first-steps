package org.sii.module4.lesson.extend;

public class Labrador extends Dog
{
    public Labrador(String name)
    {
        super(name);
    }

    public Labrador()
    {
        System.out.println("In Labrador class");
    }
}
