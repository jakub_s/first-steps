package org.sii.module4.lesson.extend;

public class Main
{
    public static void main(String[] args)
    {
//        classConstruction();
        methodOverriding();
    }

    private static void classConstruction()
    {
        new Labrador();
    }

    private static void methodOverriding()
    {
        Labrador labrador = new Labrador("Laby");
        LazyDog lazyDog = new LazyDog("Leniuch");

        labrador.fetch();
        lazyDog.fetch();
    }
}
