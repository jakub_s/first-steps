package org.sii.module4.lesson.extend;

public class Animal
{
    public Animal()
    {
        System.out.println("In Animal class");
    }

    public Animal(String name)
    {
        this.name = name;
    }

    private String name;
    protected int age;

    public String getName()
    {
        return name;
    }
}
