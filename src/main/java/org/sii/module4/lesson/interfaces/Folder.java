package org.sii.module4.lesson.interfaces;

public class Folder implements Readable
{
    @Override
    public String read()
    {
        return "Folder content";
    }

    public void folderMethod()
    {
        System.out.println("folder method");
    }
}
