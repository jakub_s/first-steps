package org.sii.module4.lesson.interfaces;

public class Main
{
    public static void main(String[] args)
    {
        Folder folder = new Folder();
        MyFile myFile = new MyFile();

        read(folder);
        read(myFile);
        write(myFile);
        // write(folder); // WRONG folder is not Writeable
    }

    public static void read(Readable readable)
    {
        System.out.println(readable.read());
    }

    public static void write(Writeable writeable)
    {
        writeable.write("text to write");
    }
}
