package org.sii.module4.lesson.interfaces;

public interface Readable
{
    public String read();
}
