package org.sii.module4.lesson.interfaces;

public class MyFile implements Readable, Writeable
{
    @Override
    public String read()
    {
        return "file content";
    }

    @Override
    public void write(String text)
    {

    }
}
