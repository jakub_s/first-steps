package org.sii.module4.lesson.interfaces;

public interface Writeable
{
    public void write(String text);
}
