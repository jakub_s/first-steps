Task 1
Do klasy 'Operations' dodaj:
- prywatne pole type 'int' o nazwie 'amount'
- prywatne pole type 'String' o nazwie 'name'
- publiczną metode 'setAmount', która przyjmuje jeden argument typu 'int', nic nie zwraca oraz przypisuje otrzymany argument do pola 'amount'
- publiczną metode 'setName', która przyjmuje jeden argument typu 'String', nic nie zwraca oraz przypisuje otrzymany argument do pola 'name'
- publiczną metode 'getAmount', która nie przyjmuje żadnych argumentów oraz zwraca wartość pola 'amount'
- publiczną metode 'getName', która nie przyjmuje żadnych argumentów oraz zwraca wartość pola 'name'

Task 2
Do klasy 'Operations' dodaj:
- prywatne pole typu 'MyValue' (juz istniejąca klasa obok 'Operations') o nazwie 'myValue'
- publiczną metode 'setMyValue', która przyjmuje jeden argument typu 'MyValue', nic nie zwraca oraz przypisuje otrzymany argument do pola 'myValue'
- publiczną metode 'getMyValue', która nie przyjmuje żadnych argumentów oraz zwraca wartość pola 'myValue'

Task 3
Do klasy 'Operations' dodaj:
    - konstruktor przyjmujący trzy argumenty, typu 'int', 'String', oraz 'MyValue'.
W konstruktorze przypisz otrzymane argumenty do pól 'amount', 'name' oraz 'myValue'.
Po wykonaniu zadania upewnij się że pozostałe testy nadal przechodzą!