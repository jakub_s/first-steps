Task 1
Do klasy Relations dodaj
- publiczną metode 'areEqual', która:
    * przyjmuje dwa argumenty typu 'int'
    * zwraca typ 'boolean' - wartośc 'true', jeśli otrzymane argumenty są równe sobie, 'false' w przeciwnym wypadku
- publiczną metode 'areNotEqual', która:
    * przyjmuje dwa argumenty typu 'int'
    * zwraca typ 'boolean' - wartośc 'true', jeśli otrzymane argumenty są różne od siebie, 'false' w przeciwnym wypadku
- publiczną metode 'firstIsGreater', która:
    * przyjmuje dwa argumenty typu 'int'
    * zwraca typ 'boolean' - wartośc 'true', jeśli pierwszy z otrzymanych argumentów jest wiekszy niż drugi, 'false' w przeciwnym wypadku
- publiczną metode 'secondIsGreaterOfEqual', która:
    * przyjmuje dwa argumenty typu 'int'
    * zwraca typ 'boolean' - wartośc 'true', jeśli drugi z otrzymanych argumentów jest wiekszy lub równy pirwszemu, 'false' w przeciwnym wypadku
- publiczną metode 'isWithin', która:
    * przyjmuje trzy argumenty typu 'int'
    * zwraca typ 'boolean' - wartośc 'true', jeśli pierwszy argument jest większy lub równy drugiem i mniejszy lub równy trzeciemu, 'false' w przeciwnym wypadku
- publiczną metode 'isOutside', która:
    * przyjmuje trzy argumenty typu 'int'
    * zwraca typ 'boolean' - wartośc 'true', jeśli pierwszy argument jest mniejszy lub równy drugiem lub większy lub równy trzeciemu, 'false' w przeciwnym wypadku


