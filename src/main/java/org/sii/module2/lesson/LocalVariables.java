package org.sii.module2.lesson;

// https://www.geeksforgeeks.org/operators-in-java/<
public class LocalVariables
{
    private int value = 12;

    public int case1()
    {
        return value;
    }

    public int case2()
    {
        int value = 2;

        return value;
    }

    public int case3()
    {
        int value = 2;

        return this.value;
    }

    public int case4()
    {
        int value = 2;
        int another = 4;

        value = another;

        return value;
    }

    public int case5()
    {
        int value = 2;
        int another = 4;

        value = another;

        another = another + 1;

        return value;
    }

    public static void main(String[] args)
    {
        LocalVariables localVariables = new LocalVariables();

        System.out.println("case1: " + localVariables.case1());
        System.out.println("case2: " + localVariables.case2());
        System.out.println("case3: " + localVariables.case3());
        System.out.println("case4: " + localVariables.case4());
        System.out.println("case5: " + localVariables.case5());
    }

}
